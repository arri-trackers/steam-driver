#ifndef ARRI_TRACKERS_UTIL_HPP_
#define ARRI_TRACKERS_UTIL_HPP_

#include <tuple>

namespace ArriTrackers {

namespace Util
{
	float map(float x, std::tuple<float, float> from, std::tuple<float, float> to);
}

}

#endif // ARRI_TRACKERS_UTIL_HPP_