#include "Util.hpp"

using namespace ArriTrackers;

float Util::map(float x, std::tuple<float, float> from, std::tuple<float, float> to)
{
    float toRange = std::get<1>(to) - std::get<0>(to);
    float fromRange = std::get<1>(from) - std::get<0>(from);
    return (x - std::get<0>(from))*toRange/fromRange + std::get<0>(to);
}