#include "Server.hpp"

#include "packet_handler/Ping.hpp"
#include "packet_handler/EnabledTrackers.hpp"
#include "packet_handler/TrackerTransform.hpp"

#include <iostream>
#include <thread>
#include <Windows.h>

using namespace ArriTrackers;
using namespace std::chrono_literals;

class DummyTrackerManager : public ITrackerManager
{
public:
    DummyTrackerManager();
    virtual std::shared_ptr<TrackerDriver> GetTracker(unsigned int index) override;

private:
	std::array<std::shared_ptr<TrackerDriver>, 12> _trackers;

	void _InitTrackers();
	void _InitTracker(unsigned int index, const std::string &name);
	void _InitLeftFoot();
	void _InitRightFoot();
	void _InitLeftShin();
	void _InitRightShin();
	void _InitLeftThigh();
	void _InitRightThigh();
	void _InitWaist();
	void _InitChest();
	void _InitLeftShoulder();
	void _InitRightShoulder();
	void _InitLeftUpperArm();
	void _InitRightUpperArm();
};

DummyTrackerManager::DummyTrackerManager()
{
    _InitTrackers();
}

std::shared_ptr<TrackerDriver> DummyTrackerManager::GetTracker(unsigned int index)
{
    return _trackers.at(index);
}

void DummyTrackerManager::_InitTrackers()
{
	_InitLeftFoot();
	_InitRightFoot();
	_InitLeftShin();
	_InitRightShin();
	_InitLeftThigh();
	_InitRightThigh();
	_InitWaist();
	_InitChest();
	_InitLeftShoulder();
	_InitRightShoulder();
	_InitLeftUpperArm();
	_InitRightUpperArm();
}

void DummyTrackerManager::_InitTracker(unsigned int index, const std::string &name)
{
	_trackers[index] = std::make_unique<TrackerDriver>();
	_trackers[index]->SetTrackerIndex(index);
	_trackers[index]->SetModel(name);
	_trackers[index]->SetVersion(ATSD_VERSION);
}

void DummyTrackerManager::_InitLeftFoot()
{
	_InitTracker(LFOOT, "lfoot");
}

void DummyTrackerManager::_InitRightFoot()
{
	_InitTracker(RFOOT, "rfoot");
}

void DummyTrackerManager::_InitLeftShin()
{
	_InitTracker(LSHIN, "llowerleg");
}

void DummyTrackerManager::_InitRightShin()
{
	_InitTracker(RSHIN, "rlowerleg");
}

void DummyTrackerManager::_InitLeftThigh()
{
	_InitTracker(LTHIGH, "lthigh");
}

void DummyTrackerManager::_InitRightThigh()
{
	_InitTracker(RTHIGH, "rthigh");
}

void DummyTrackerManager::_InitWaist()
{
	_InitTracker(WAIST, "waist");
}

void DummyTrackerManager::_InitChest()
{
	_InitTracker(CHEST, "chest");
}

void DummyTrackerManager::_InitLeftShoulder()
{
	_InitTracker(LSHOULDER, "lshoulder");
}

void DummyTrackerManager::_InitRightShoulder()
{
	_InitTracker(RSHOULDER, "rshoulder");
}

void DummyTrackerManager::_InitLeftUpperArm()
{
	_InitTracker(LUPARM, "lupperarm");
}

void DummyTrackerManager::_InitRightUpperArm()
{
	_InitTracker(RUPARM, "rupperarm");
}

int main()
{
    std::cout << "Test...\n";

    DummyTrackerManager dtm;
    Server server(dtm);
    server.Open("0.0.0.0", 45101);
    server.SetPacketHandler(0x00, std::make_shared<packet_handler::Ping>());
    server.SetPacketHandler(0x01, std::make_shared<packet_handler::EnabledTrackers>(dtm));
    server.SetPacketHandler(0x02, std::make_shared<packet_handler::TrackerTransform>(dtm));

    while(!(GetAsyncKeyState(VK_ESCAPE) & 0x8000)) {
        // std::cout << "Tick\n";
        server.Tick();
        std::this_thread::sleep_for(50ms);
    }

    return 0;
}