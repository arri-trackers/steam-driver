#ifndef ARRI_TRACKERS_DEVICE_FACTORY_HPP_
#define ARRI_TRACKERS_DEVICE_FACTORY_HPP_

#include "DeviceProvider.hpp"

namespace ArriTrackers {
    DeviceProvider& GetDeviceProvider();
}

#endif // ARRI_TRACKERS_DEVICE_FACTORY_HPP_