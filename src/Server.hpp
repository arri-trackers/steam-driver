#ifndef ARRI_TRACKERS_SERVER_HPP_
#define ARRI_TRACKERS_SERVER_HPP_

#include "Connection.hpp"
#include "ITrackerManager.hpp"
#include "PacketHandler.hpp"

#include <string>
#include <span>
#include <functional>
#include <map>
#include <asio.hpp>

// Boundary server class to the networking library

namespace ArriTrackers {

class Server
{
public:
    Server(ITrackerManager &trackerManager);

    virtual ~Server();

    void Open(const std::string &ip, int port);

    void Close();

    void Tick();

    ITrackerManager& GetTrackerManager();

    void SetPacketHandler(unsigned char command, std::shared_ptr<PacketHandler> handler);

private:
    asio::io_context _ioContext;
    std::shared_ptr<asio::ip::tcp::acceptor> _acceptor;
    std::vector<std::shared_ptr<Connection>> _clients;
    ITrackerManager &_trackerManager;
    std::map<unsigned char, std::shared_ptr<PacketHandler>> _packetHandlers;

    void _WaitForConnection();
    void _UpdateNextClient(std::vector<std::shared_ptr<Connection>>::iterator &it);
    void _HandlePacket(Connection &con, const Packet &packet);
};

}

#endif // ARRI_TRACKERS_SERVER_HPP_