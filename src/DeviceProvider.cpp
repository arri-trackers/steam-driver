#include "DeviceProvider.hpp"
#include "packet_handler/Ping.hpp"
#include "packet_handler/EnabledTrackers.hpp"
#include "packet_handler/TrackerTransform.hpp"

using namespace vr;
using namespace ArriTrackers;

#ifndef ATSD_VERSION
	#define ATSD_VERSION "?"
#endif

DeviceProvider::DeviceProvider():
	_server(*this)
{
    _server.SetPacketHandler(0x00, std::make_shared<packet_handler::Ping>());
    _server.SetPacketHandler(0x01, std::make_shared<packet_handler::EnabledTrackers>(*this));
    _server.SetPacketHandler(0x02, std::make_shared<packet_handler::TrackerTransform>(*this));
}

EVRInitError DeviceProvider::Init(IVRDriverContext* pDriverContext)
{
	VR_INIT_SERVER_DRIVER_CONTEXT(pDriverContext);
	_InitTrackers();
	_server.Open("0.0.0.0", 45101);
	return VRInitError_None;
}

void DeviceProvider::Cleanup()
{
	for(auto &tracker : _trackers)
		if(tracker)
			tracker.reset();
}

const char* const* DeviceProvider::GetInterfaceVersions()
{
	return k_InterfaceVersions;
}

void DeviceProvider::RunFrame()
{
	_server.Tick();
	for(auto &tracker : _trackers)
		if(tracker)
			tracker->RunFrame();
}

bool DeviceProvider::ShouldBlockStandbyMode()
{
	return false;
}

void DeviceProvider::EnterStandby()
{
}

void DeviceProvider::LeaveStandby()
{
}

std::shared_ptr<TrackerDriver> DeviceProvider::GetTracker(unsigned int index)
{
	return _trackers.at(index);
}

void DeviceProvider::_InitTrackers()
{
	_InitLeftFoot();
	_InitRightFoot();
	_InitLeftShin();
	_InitRightShin();
	_InitLeftThigh();
	_InitRightThigh();
	_InitWaist();
	_InitChest();
	_InitLeftShoulder();
	_InitRightShoulder();
	_InitLeftUpperArm();
	_InitRightUpperArm();
}

void DeviceProvider::_InitTracker(unsigned int index, const std::string &name)
{
	_trackers[index] = std::make_unique<TrackerDriver>();
	_trackers[index]->SetTrackerIndex(index);
	_trackers[index]->SetModel(name);
	_trackers[index]->SetVersion(ATSD_VERSION);
	VRServerDriverHost()->TrackedDeviceAdded(
		name.c_str(), TrackedDeviceClass_GenericTracker, _trackers[index].get());
}

void DeviceProvider::_InitLeftFoot()
{
	_InitTracker(LFOOT, "lfoot");
}

void DeviceProvider::_InitRightFoot()
{
	_InitTracker(RFOOT, "rfoot");
}

void DeviceProvider::_InitLeftShin()
{
	_InitTracker(LSHIN, "llowerleg");
}

void DeviceProvider::_InitRightShin()
{
	_InitTracker(RSHIN, "rlowerleg");
}

void DeviceProvider::_InitLeftThigh()
{
	_InitTracker(LTHIGH, "lthigh");
}

void DeviceProvider::_InitRightThigh()
{
	_InitTracker(RTHIGH, "rthigh");
}

void DeviceProvider::_InitWaist()
{
	_InitTracker(WAIST, "waist");
}

void DeviceProvider::_InitChest()
{
	_InitTracker(CHEST, "chest");
}

void DeviceProvider::_InitLeftShoulder()
{
	_InitTracker(LSHOULDER, "lshoulder");
}

void DeviceProvider::_InitRightShoulder()
{
	_InitTracker(RSHOULDER, "rshoulder");
}

void DeviceProvider::_InitLeftUpperArm()
{
	_InitTracker(LUPARM, "lupperarm");
}

void DeviceProvider::_InitRightUpperArm()
{
	_InitTracker(RUPARM, "rupperarm");
}