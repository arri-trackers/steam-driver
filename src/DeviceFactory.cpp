#include "DeviceFactory.hpp"

using namespace vr;
using namespace ArriTrackers;

#if defined(_WIN32)
	#define HMD_DLL_EXPORT extern "C" __declspec( dllexport )
	#define HMD_DLL_IMPORT extern "C" __declspec( dllimport )
#elif defined(__GNUC__) || defined(COMPILER_GCC) || defined(__APPLE__)
	#define HMD_DLL_EXPORT extern "C" __attribute__((visibility("default")))
	#define HMD_DLL_IMPORT extern "C"
#else
	#error "Unsupported Platform."
#endif

static DeviceProvider _deviceProvider;

HMD_DLL_EXPORT void* HmdDriverFactory(const char* interfaceName, int* returnCode)
{
	if(std::strcmp(interfaceName, vr::IServerTrackedDeviceProvider_Version) == 0)
		return &_deviceProvider;
	if(returnCode)
		*returnCode = vr::VRInitError_Init_InterfaceNotFound;
	return nullptr;
}

DeviceProvider& ArriTrackers::GetDeviceProvider()
{
	return _deviceProvider;
}