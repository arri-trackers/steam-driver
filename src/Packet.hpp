#ifndef ARRI_TRACKERS_PACKET_HPP_
#define ARRI_TRACKERS_PACKET_HPP_

#include <vector>
#include <span>

namespace ArriTrackers {

struct Packet
{
    unsigned char command;
    std::vector<unsigned char> data;

    std::vector<unsigned char> GetBytes() const;
    void InitFromBytes(std::span<unsigned char> bytes);
    size_t GetNumBytes();
};

}

#endif // ARRI_TRACKERS_PACKET_HPP_