#ifndef ARRI_TRACKERS_I_TRACKER_MANAGER_HPP_
#define ARRI_TRACKERS_I_TRACKER_MANAGER_HPP_

#include "TrackerDriver.hpp"

#include <memory>

namespace ArriTrackers {

class ITrackerManager
{
public:
	enum
	{
		LFOOT = 0,
		RFOOT,
		LSHIN,
		RSHIN,
		LTHIGH,
		RTHIGH,
		WAIST,
		CHEST,
		LSHOULDER,
		RSHOULDER,
		LUPARM,
		RUPARM
	};

    virtual std::shared_ptr<TrackerDriver> GetTracker(unsigned int index) = 0;

	bool HasTracker(unsigned int index)
		{ return GetTracker(index) != nullptr; }
};

}

#endif // ARRI_TRACKERS_I_TRACKER_MANAGER_HPP_