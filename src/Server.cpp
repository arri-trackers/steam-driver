#include "Server.hpp"

#include <asio.hpp>
#include <iostream>

using namespace ArriTrackers;
using asio::ip::tcp;

Server::Server(ITrackerManager &trackerManager):
    _trackerManager(trackerManager)
{
}

Server::~Server()
{
}

void Server::Open(const std::string &ip, int port)
{
    tcp::resolver resolver(_ioContext);
    tcp::resolver::results_type endpoints = resolver.resolve(ip, std::to_string(port));
    _acceptor = std::make_shared<tcp::acceptor>(_ioContext, *endpoints);
    _WaitForConnection();
}

void Server::Close()
{
    _acceptor.reset();
    for(auto &client : _clients)
        client->Close();
}

void Server::Tick()
{
    _ioContext.poll();
    for(auto it = _clients.begin(); it != _clients.end();)
        _UpdateNextClient(it);
}

ITrackerManager& Server::GetTrackerManager()
{
    return _trackerManager;
}

void Server::SetPacketHandler(unsigned char command, std::shared_ptr<PacketHandler> handler)
{
    if(handler != nullptr)
        _packetHandlers[command] = handler;
    else
        _packetHandlers.erase(command);
}

void Server::_WaitForConnection()
{
    _acceptor->async_accept([this](std::error_code ec, tcp::socket socket) {
        if(!ec) {
            std::cout << "New connection: " << socket.remote_endpoint() << '\n';
            auto con = std::make_shared<Connection>(std::move(socket));
            _clients.push_back(con);
            con->SetPacketListener([this](Connection &con, const Packet &packet){
                _HandlePacket(con, packet);
            });
        } else
            std::cerr << "Accept error: " << ec.message() << '\n';
        _WaitForConnection();
    });
}

void Server::_UpdateNextClient(std::vector<std::shared_ptr<Connection>>::iterator &it)
{
    std::shared_ptr<Connection> con = *it;
    if(!con->IsAlive()) {
        std::cout << "Client disconnected.\n";
        it = _clients.erase(it);
    } else {
        con->Tick();
        ++it;
    }
}

void Server::_HandlePacket(Connection &con, const Packet &packet)
{
    if(_packetHandlers.find(packet.command) != _packetHandlers.end())
        _packetHandlers[packet.command]->handle(con, packet);
    else
        std::cout << "Warning: Unhandled packet !" << (unsigned int)packet.command << '\n';
}