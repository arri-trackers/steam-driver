#ifndef ARRI_TRACKERS_DEVICE_PROVIDER_HPP_
#define ARRI_TRACKERS_DEVICE_PROVIDER_HPP_

#include "TrackerDriver.hpp"
#include "ITrackerManager.hpp"
#include "Server.hpp"

#include <openvr_driver.h>
#include <thread>
#include <array>

namespace ArriTrackers {

class DeviceProvider : public IServerTrackedDeviceProvider, public ITrackerManager
{
public:
	DeviceProvider();

	virtual EVRInitError Init(IVRDriverContext* pDriverContext);
	virtual void Cleanup();
	virtual const char* const* GetInterfaceVersions();
	virtual void RunFrame();
	virtual bool ShouldBlockStandbyMode();
	virtual void EnterStandby();
	virtual void LeaveStandby();

	virtual std::shared_ptr<TrackerDriver> GetTracker(unsigned int index) override;

private:
	std::array<std::shared_ptr<TrackerDriver>, 12> _trackers;
	Server _server;

	void _InitTrackers();
	void _InitTracker(unsigned int index, const std::string &name);
	void _InitLeftFoot();
	void _InitRightFoot();
	void _InitLeftShin();
	void _InitRightShin();
	void _InitLeftThigh();
	void _InitRightThigh();
	void _InitWaist();
	void _InitChest();
	void _InitLeftShoulder();
	void _InitRightShoulder();
	void _InitLeftUpperArm();
	void _InitRightUpperArm();
};

}

#endif // ARRI_TRACKERS_DEVICE_PROVIDER_HPP_