#ifndef ARRI_TRACKERS_PACKET_HANDLER_HPP_
#define ARRI_TRACKERS_PACKET_HANDLER_HPP_

#include "Packet.hpp"
#include "Connection.hpp"

namespace ArriTrackers {

class PacketHandler
{
public:
    virtual void handle(Connection &con, const Packet &packet) = 0;
};

}

#endif // ARRI_TRACKERS_PACKET_HANDLER_HPP_