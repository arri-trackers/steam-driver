#ifndef ARRI_TRACKERS_TRACKER_DRIVER_HPP_
#define ARRI_TRACKERS_TRACKER_DRIVER_HPP_

#include <openvr_driver.h>

using namespace vr;

namespace ArriTrackers {

class TrackerDriver : public ITrackedDeviceServerDriver
{
public:
	TrackerDriver();

	virtual void SetTrackerIndex(int32_t trackerIndex);
	virtual void SetModel(const std::string &model);
	virtual void SetVersion(const std::string &version);
	virtual void PowerOff();
	void RunFrame();

	virtual EVRInitError Activate(TrackedDeviceIndex_t unObjectId) override;
	virtual void Deactivate() override;
	virtual void EnterStandby() override;
	void* GetComponent(const char* pchComponentNameAndVersion) override;
	virtual void DebugRequest(
		const char* pchRequest, char* pchResponseBuffer, uint32_t unResponseBufferSize) override;
	virtual DriverPose_t GetPose() override;

	void Enable();
	void Disable();
	void SetEnabled(bool flag);
	void SetPosition(HmdVector3d_t position);
	void SetRotation(HmdQuaternion_t rotation);

private:
	int32_t _trackerIndex;
	std::string _model;
	std::string _version;
	TrackedDeviceIndex_t _unObjectId;
	PropertyContainerHandle_t _ulPropertyContainer;
	bool _enabled;
	HmdVector3d_t _position;
	HmdQuaternion_t _rotation;

	void _SetupProperties();

	void _SetInt32Property(ETrackedDeviceProperty prop, int32_t newValue);
	void _SetUint64Property(ETrackedDeviceProperty prop, uint64_t newValue);
	void _SetBoolProperty(ETrackedDeviceProperty prop, bool newValue);
	void _SetStringProperty(ETrackedDeviceProperty prop, const std::string &newValue);
};

}

#endif // ARRI_TRACKERS_TRACKER_DRIVER_HPP_