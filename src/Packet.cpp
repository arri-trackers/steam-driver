#include "Packet.hpp"

#include <stdexcept>

using namespace ArriTrackers;

std::vector<unsigned char> Packet::GetBytes() const
{
    std::vector<unsigned char> bytes;
    bytes.reserve(2+data.size());
    bytes.push_back(data.size() & 0xFF);
    bytes.push_back(command);
    bytes.insert(bytes.end(), data.begin(), data.end());
    return bytes;
}

void Packet::InitFromBytes(std::span<unsigned char> bytes)
{
    unsigned char len = bytes[0];
    if(2+len > bytes.size())
        throw std::runtime_error("Invalid packet");
    command = bytes[1];
    data.clear();
    data.reserve(2+len);
    data.insert(data.end(), bytes.begin()+2, bytes.begin()+2+len);
}

size_t Packet::GetNumBytes()
{
    return 2+data.size();
}