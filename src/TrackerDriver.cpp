#include "TrackerDriver.hpp"

using namespace ArriTrackers;
using namespace vr;


TrackerDriver::TrackerDriver():
	_trackerIndex(-1),
	_unObjectId(k_unTrackedDeviceIndexInvalid),
	_ulPropertyContainer(k_ulInvalidPropertyContainer),
	_enabled(false),
	_position{0, 0, 0},
	_rotation{0, 0, 0, 1}
{
}

void TrackerDriver::SetTrackerIndex(int32_t trackerIndex)
{
	_trackerIndex = trackerIndex;
}

void TrackerDriver::SetModel(const std::string &model)
{
	_model = model;
}

void TrackerDriver::SetVersion(const std::string &version)
{
	_version = version;
}

EVRInitError TrackerDriver::Activate(TrackedDeviceIndex_t unObjectId)
{
	_unObjectId = unObjectId;
	_ulPropertyContainer = VRProperties()->TrackedDeviceToPropertyContainer(_unObjectId);
	_SetupProperties();
	return VRInitError_None;
}

void TrackerDriver::Deactivate()
{
	_unObjectId = k_unTrackedDeviceIndexInvalid;
}

void TrackerDriver::EnterStandby()
{
}

void* TrackerDriver::GetComponent(const char* pchComponentNameAndVersion)
{
	if(!_stricmp(pchComponentNameAndVersion, IVRDisplayComponent_Version))
		return dynamic_cast<IVRDisplayComponent*>(this);
	return nullptr;
}

void TrackerDriver::PowerOff()
{
}

void TrackerDriver::DebugRequest(
	const char* pchRequest, char* pchResponseBuffer, uint32_t unResponseBufferSize)
{
	if(unResponseBufferSize >= 1)
		pchResponseBuffer[0] = 0;
}

DriverPose_t TrackerDriver::GetPose()
{
	DriverPose_t pose = { 0 };
	pose.poseIsValid = _enabled;
	pose.result = _enabled ? TrackingResult_Running_OK : TrackingResult_Running_OutOfRange;
	pose.deviceIsConnected = _enabled;
	pose.qWorldFromDriverRotation = {1, 0, 0, 0};
	pose.qDriverFromHeadRotation = {1, 0, 0, 0};
	pose.vecPosition[0] = _position.v[0];
	pose.vecPosition[1] = _position.v[1];
	pose.vecPosition[2] = _position.v[2];
	pose.qRotation = _rotation;
	return pose;
}

void TrackerDriver::RunFrame()
{
	if(_unObjectId != k_unTrackedDeviceIndexInvalid)
		VRServerDriverHost()->TrackedDevicePoseUpdated(
			_unObjectId, GetPose(), sizeof(DriverPose_t));
}

void TrackerDriver::Enable()
{
	_enabled = true;
}

void TrackerDriver::Disable()
{
	_enabled = false;
}

void TrackerDriver::SetEnabled(bool flag)
{
	_enabled = flag;
}

void TrackerDriver::SetPosition(HmdVector3d_t position)
{
	_position = position;
}

void TrackerDriver::SetRotation(HmdQuaternion_t rotation)
{
	_rotation = rotation;
}

void TrackerDriver::_SetupProperties()
{
	_SetStringProperty(Prop_TrackingSystemName_String, "ArriTrackers");
	_SetStringProperty(Prop_ManufacturerName_String, "ArriTrackers");
	_SetInt32Property(Prop_ControllerRoleHint_Int32, TrackedControllerRole_OptOut);
	_SetStringProperty(Prop_ModelNumber_String, _model);
	_SetStringProperty(Prop_SerialNumber_String, _model);
	_SetStringProperty(Prop_RenderModelName_String, "{arritrackers}frame");
	_SetBoolProperty(Prop_WillDriftInYaw_Bool, false);
	_SetStringProperty(Prop_TrackingFirmwareVersion_String, _version);
	_SetStringProperty(Prop_HardwareRevision_String, _version);
	_SetStringProperty(Prop_ResourceRoot_String, "arritrackers");
	_SetStringProperty(Prop_RegisteredDeviceType_String, "arritrackers/" + _model);
	_SetStringProperty(
		Prop_InputProfilePath_String, "{arritrackers}/input/arritrackers_profile.json");
	_SetBoolProperty(Prop_NeverTracked_Bool, false);
	_SetUint64Property(Prop_CurrentUniverseId_Uint64, 2);
	_SetBoolProperty(Prop_HasDisplayComponent_Bool, false);
	_SetBoolProperty(Prop_HasCameraComponent_Bool, false);
	_SetBoolProperty(Prop_HasDriverDirectModeComponent_Bool, false);
	_SetBoolProperty(Prop_HasVirtualDisplayComponent_Bool, false);
	_SetInt32Property(Prop_ControllerHandSelectionPriority_Int32, -1);
}

void TrackerDriver::_SetInt32Property(ETrackedDeviceProperty prop, int32_t newValue)
{
	VRProperties()->SetInt32Property(_ulPropertyContainer, prop, newValue);
}

void TrackerDriver::_SetUint64Property(ETrackedDeviceProperty prop, uint64_t newValue)
{
	VRProperties()->SetUint64Property(_ulPropertyContainer, prop, newValue);
}

void TrackerDriver::_SetBoolProperty(ETrackedDeviceProperty prop, bool newValue)
{
	VRProperties()->SetBoolProperty(_ulPropertyContainer, prop, newValue);
}

void TrackerDriver::_SetStringProperty(ETrackedDeviceProperty prop, const std::string &newValue)
{
	VRProperties()->SetStringProperty(_ulPropertyContainer, prop, newValue.c_str());
}