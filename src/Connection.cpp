#include "Connection.hpp"

#include <iostream>

using namespace ArriTrackers;


Connection::Connection(asio::ip::tcp::socket socket):
    _socket(std::move(socket)),
    _eofReceived(false)
{
    _socket.set_option(asio::socket_base::keep_alive(true));
    _socket.non_blocking(true);
}

Connection::~Connection()
{
}

void Connection::Send(const std::string &data)
{
    _socket.write_some(asio::buffer(data.data(), data.size()));
}

void Connection::Send(std::span<unsigned char> data)
{
    _socket.write_some(asio::buffer(data.data(), data.size()));
}

void Connection::Send(const Packet &packet)
{
    std::vector<unsigned char> bytes = packet.GetBytes();
    Send(std::span(bytes.begin(), bytes.end()));
}

void Connection::Tick()
{
    std::vector<unsigned char> buffer = _Read();

    // if(buffer.size() > 0) {
    //     printf("Data received:");
    //     for(unsigned char c : buffer)
    //         printf(" 0x%02x", c);
    //     printf("\n");
    // }

    for(size_t offset = 0; offset < buffer.size();) {
        Packet p;
        try {
            p.InitFromBytes(std::span(buffer.data()+offset, buffer.size()-offset));
        } catch(...) {
            // printf("Error occured (InitFromBytes)\n");
            break;
        }
        _packetListener(*this, p);
        offset += p.GetNumBytes();
    }
}

void Connection::Close()
{
    _socket.close();
}

void Connection::SetPacketListener(std::function<void(Connection&, const Packet&)> listener)
{
    _packetListener = listener;
}

bool Connection::IsAlive() const
{
    return !_eofReceived;
}

std::vector<unsigned char> Connection::_ConvertStreamBufToVec(const asio::streambuf &buf)
{
    std::vector<unsigned char> outBuf(buf.size());
    asio::buffer_copy(asio::buffer(outBuf), buf.data());
    return outBuf;
}

std::vector<unsigned char> Connection::_Read()
{
    asio::streambuf response;
    asio::error_code ec;
    size_t nRead = asio::read(_socket, response, ec);
    if(ec == asio::error::eof)
        _eofReceived = true;
    return _ConvertStreamBufToVec(response);
}