#include "EnabledTrackers.hpp"

using namespace ArriTrackers::packet_handler;

EnabledTrackers::EnabledTrackers(ITrackerManager &trackerManager):
    _trackerManager(trackerManager)
{
}

void EnabledTrackers::handle(Connection &con, const Packet &packet)
{
    if(packet.data.size() != 2)
        return;
    uint16_t flags = (static_cast<uint16_t>(packet.data[0]) << 8) | packet.data[1];
    for(unsigned int i = 0; i < 12; ++i) {
        // if(flags & (1 << i))
        //     printf("Tracker %u enabled.\n", i);
        // else
        //     printf("Tracker %u disabled.\n", i);
        _trackerManager.GetTracker(i)->SetEnabled((flags & (1 << i)) > 0);
    }
}