#ifndef ARRI_TRACKERS_PACKET_HANDLER_TRACKER_TRANSFORM_HPP_
#define ARRI_TRACKERS_PACKET_HANDLER_TRACKER_TRANSFORM_HPP_

#include "../PacketHandler.hpp"
#include "../ITrackerManager.hpp"

namespace ArriTrackers {
namespace packet_handler {

class TrackerTransform : public PacketHandler
{
public:
    TrackerTransform(ITrackerManager &trackerManager);

    virtual void handle(Connection &con, const Packet &packet) override;

private:
    ITrackerManager &_trackerManager;
};

}
}

#endif // ARRI_TRACKERS_PACKET_HANDLER_TRACKER_TRANSFORM_HPP_