#ifndef ARRI_TRACKERS_PACKET_HANDLER_PING_HPP_
#define ARRI_TRACKERS_PACKET_HANDLER_PING_HPP_

#include "../PacketHandler.hpp"

namespace ArriTrackers {
namespace packet_handler {

class Ping : public PacketHandler
{
public:
    virtual void handle(Connection &con, const Packet &packet) override;
};

}
}

#endif // ARRI_TRACKERS_PACKET_HANDLER_PING_HPP_