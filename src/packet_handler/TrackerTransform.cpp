#include "TrackerTransform.hpp"

using namespace ArriTrackers::packet_handler;

static float _ReadFloat(const std::vector<unsigned char> &buf, unsigned int offset)
{
    const unsigned char *bptr = buf.data() + offset;
    const float *fptr = reinterpret_cast<const float*>(bptr);
    return *fptr;
}

TrackerTransform::TrackerTransform(ITrackerManager &trackerManager):
    _trackerManager(trackerManager)
{
}

void TrackerTransform::handle(Connection &con, const Packet &packet)
{
    if(packet.data.size() != 29)
        return;
    unsigned char index = packet.data[0];
    float posX = _ReadFloat(packet.data, 1);
    float posY = _ReadFloat(packet.data, 5);
    float posZ = _ReadFloat(packet.data, 9);
    float rotX = _ReadFloat(packet.data, 13);
    float rotY = _ReadFloat(packet.data, 17);
    float rotZ = _ReadFloat(packet.data, 21);
    float rotW = _ReadFloat(packet.data, 25);
    // printf("Tracker transform:\n");
    // printf("  Position: [%f, %f, %f]\n", posX, posY, posZ);
    // printf("  Rotation: [%f, %f, %f, %f]\n", rotW, rotX, rotY, rotZ);
    std::shared_ptr<TrackerDriver> tracker = _trackerManager.GetTracker(index);
    tracker->SetPosition({posX, posY, posZ});
    tracker->SetRotation({rotW, rotX, rotY, rotZ});
}