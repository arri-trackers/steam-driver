#ifndef ARRI_TRACKERS_PACKET_HANDLER_ENABLED_TRACKERS_HPP_
#define ARRI_TRACKERS_PACKET_HANDLER_ENABLED_TRACKERS_HPP_

#include "../PacketHandler.hpp"
#include "../ITrackerManager.hpp"

namespace ArriTrackers {
namespace packet_handler {

class EnabledTrackers : public PacketHandler
{
public:
    EnabledTrackers(ITrackerManager &trackerManager);

    virtual void handle(Connection &con, const Packet &packet) override;

private:
    ITrackerManager &_trackerManager;
};

}
}

#endif // ARRI_TRACKERS_PACKET_HANDLER_ENABLED_TRACKERS_HPP_