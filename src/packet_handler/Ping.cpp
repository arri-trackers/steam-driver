#include "Ping.hpp"

#include <iostream>

using namespace ArriTrackers::packet_handler;

void Ping::handle(Connection &con, const Packet &packet)
{
    std::cout << "Ping packet received\n";
    Packet p;
    p.command = packet.command;
    con.Send(p);
}