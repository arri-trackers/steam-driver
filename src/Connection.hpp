#ifndef ARRI_TRACKERS_CONNECTION_HPP_
#define ARRI_TRACKERS_CONNECTION_HPP_

#include "Packet.hpp"

#include <asio.hpp>
#include <span>
#include <string>

namespace ArriTrackers {

class Connection
{
public:
    Connection(asio::ip::tcp::socket socket);
    virtual ~Connection();

    void Send(const std::string &data);

    void Send(std::span<unsigned char> data);

    void Send(const Packet &packet);

    void Tick();

    void Close();

    void SetPacketListener(std::function<void(Connection&, const Packet&)> listener);

    bool IsAlive() const;

private:
    static std::vector<unsigned char> _ConvertStreamBufToVec(const asio::streambuf &buf);

    asio::ip::tcp::socket _socket;
    std::function<void(Connection&, const Packet&)> _packetListener;
    bool _eofReceived;

    std::vector<unsigned char> _Read();
};

}

#endif // ARRI_TRACKERS_CONNECTION_HPP_